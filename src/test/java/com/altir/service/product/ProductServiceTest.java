package com.altir.service.product;

import com.altir.persistence.product.*;
import com.altir.persistence.product.entity.*;
import com.altir.service.exception.*;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.*;
import org.mockito.*;
import org.mockito.junit.jupiter.*;

import java.util.*;

import static java.util.Arrays.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
@ExtendWith(MockitoExtension.class)
class ProductServiceTest {

    @Mock
    private ProductRepository productRepository;

    @Mock
    private ProductCountInWarehouseRepository productCountInWarehouseRepository;

    @InjectMocks
    private ProductService productService;

    @Test
    void findByIdOrThrowException_existingId_ok() {
        // given
        Product expected = Product.builder().id(1).title("Test").build();

        // when
        when(productRepository.findById(1L)).thenReturn(Optional.of(expected));
        Product actual = productService.findByIdOrThrowException(1);

        // then
        assertEquals(expected, actual);
    }

    @Test
    void findByIdOrThrowException_notExistingId_existing() {
        // when
        when(productRepository.findById(1L)).thenReturn(Optional.empty());

        // then
        assertThrows(ResourceNotFoundException.class, () -> productService.findByIdOrThrowException(1));
    }

    @Test
    void findAllAvailableByProductIdOrThrowException_ok() {
        // given
        List<ProductCountInWarehouse> expected = asList(
                ProductCountInWarehouse.builder()
                        .id(ProductsCountInWarehouseCompositeKey.builder()
                                .productId(1)
                                .warehouseId(2)
                                .build())
                        .count(1)
                        .build(),
                ProductCountInWarehouse.builder()
                        .id(ProductsCountInWarehouseCompositeKey.builder()
                                .productId(1)
                                .warehouseId(3)
                                .build())
                        .count(1)
                        .build(),
                ProductCountInWarehouse.builder()
                        .id(ProductsCountInWarehouseCompositeKey.builder()
                                .productId(1)
                                .warehouseId(1)
                                .build())
                        .count(1)
                        .build()
        );

        // when
        when(productCountInWarehouseRepository.findAllAvailableByProductId(1, 1)).thenReturn(expected);
        List<ProductCountInWarehouse> actual = productService.findAllAvailableByProductIdOrThrowException(1, 1);

        // then
        assertEquals(expected, actual);
    }

    @Test
    void findAllAvailableByProductIdOrThrowException_exception() {
        // when
        when(productCountInWarehouseRepository.findAllAvailableByProductId(1, 1)).thenThrow(NoAvailableProductInWarehousesException.class);

        // then
        assertThrows(NoAvailableProductInWarehousesException.class, () -> productService.findAllAvailableByProductIdOrThrowException(1, 1));
    }

    @Test
    void updateProductCountInWarehouse_ok() {
        // given
        ProductCountInWarehouse productCountToUpdate = ProductCountInWarehouse.builder()
                .id(ProductsCountInWarehouseCompositeKey.builder()
                        .productId(1)
                        .warehouseId(3)
                        .build())
                .count(1)
                .build();

        List<ProductCountInWarehouse> productCountInWarehouses = asList(
                ProductCountInWarehouse.builder()
                        .id(ProductsCountInWarehouseCompositeKey.builder()
                                .productId(1)
                                .warehouseId(2)
                                .build())
                        .count(1)
                        .build(),
                productCountToUpdate,
                ProductCountInWarehouse.builder()
                        .id(ProductsCountInWarehouseCompositeKey.builder()
                                .productId(1)
                                .warehouseId(1)
                                .build())
                        .count(1)
                        .build()
        );

        // when
        long expectedCount = productCountToUpdate.getCount() - 1;
        productService.updateProductCountInWarehouse(productCountInWarehouses, 3);

        // then
        assertEquals(expectedCount, productCountToUpdate.getCount());
        verify(productCountInWarehouseRepository, times(1)).save(productCountToUpdate);
    }

    @Test
    void updateProductCountInWarehouse_warehouseNotInList_nothing() {
        // given
        List<ProductCountInWarehouse> productCountInWarehouses = asList(
                ProductCountInWarehouse.builder()
                        .id(ProductsCountInWarehouseCompositeKey.builder()
                                .productId(1)
                                .warehouseId(2)
                                .build())
                        .count(1)
                        .build(),
                ProductCountInWarehouse.builder()
                        .id(ProductsCountInWarehouseCompositeKey.builder()
                                .productId(1)
                                .warehouseId(3)
                                .build())
                        .count(1)
                        .build(),
                ProductCountInWarehouse.builder()
                        .id(ProductsCountInWarehouseCompositeKey.builder()
                                .productId(1)
                                .warehouseId(1)
                                .build())
                        .count(1)
                        .build()
        );

        // when
        productService.updateProductCountInWarehouse(productCountInWarehouses, 4);

        // then
        verify(productCountInWarehouseRepository, times(0)).save(any());
    }

}