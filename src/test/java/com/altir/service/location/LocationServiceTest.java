package com.altir.service.location;

import com.altir.persistence.location.*;
import com.altir.persistence.location.entity.*;
import com.altir.service.exception.*;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.*;
import org.mockito.*;
import org.mockito.junit.jupiter.*;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class LocationServiceTest {

    @Mock
    private LocationRepository locationRepository;

    @InjectMocks
    private LocationService locationService;

    @Test
    void findByIdOrThrowException_existingId_ok() {
        // given
        Location expected = Location.builder().id(1).name("Test").build();

        // when
        when(locationRepository.findById(1L)).thenReturn(Optional.of(expected));
        Location actual = locationService.findByIdOrThrowException(1);

        // then
        assertEquals(expected, actual);
    }

    @Test
    void findByIdOrThrowException_notExistingId_exception() {
        // when
        when(locationRepository.findById(1L)).thenReturn(Optional.empty());

        // then
        assertThrows(ResourceNotFoundException.class, () -> locationService.findByIdOrThrowException(1));
    }
}