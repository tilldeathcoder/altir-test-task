package com.altir.service.location;

import com.altir.service.location.model.*;
import org.junit.jupiter.api.*;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

class LocationDeliveryTimeSearcherTest {

    @Test
    void search_From_1_to_6_found_3() {
        // given
        // First group of connected locations
        LocationDeliveryTimeCommonInfo _1_to_2 = LocationDeliveryTimeCommonInfo.builder().start(1).finish(2).timeInMinutes(100).build();
        LocationDeliveryTimeCommonInfo _1_to_3 = LocationDeliveryTimeCommonInfo.builder().start(1).finish(3).timeInMinutes(200).build();
        LocationDeliveryTimeCommonInfo _2_to_3 = LocationDeliveryTimeCommonInfo.builder().start(2).finish(3).timeInMinutes(100).build();
        LocationDeliveryTimeCommonInfo _2_to_4 = LocationDeliveryTimeCommonInfo.builder().start(2).finish(4).timeInMinutes(200).build();
        LocationDeliveryTimeCommonInfo _3_to_5 = LocationDeliveryTimeCommonInfo.builder().start(3).finish(5).timeInMinutes(200).build();
        LocationDeliveryTimeCommonInfo _4_to_5 = LocationDeliveryTimeCommonInfo.builder().start(4).finish(5).timeInMinutes(100).build();
        LocationDeliveryTimeCommonInfo _5_to_6 = LocationDeliveryTimeCommonInfo.builder().start(5).finish(6).timeInMinutes(100).build();

        // Second group of connected locations. Two groups are not connected between each other!
        LocationDeliveryTimeCommonInfo _7_to_8 = LocationDeliveryTimeCommonInfo.builder().start(7).finish(8).timeInMinutes(500).build();
        LocationDeliveryTimeCommonInfo _9_to_8 = LocationDeliveryTimeCommonInfo.builder().start(9).finish(8).timeInMinutes(500).build();
        LocationDeliveryTimeCommonInfo _9_to_7 = LocationDeliveryTimeCommonInfo.builder().start(9).finish(7).timeInMinutes(500).build();

        List<LocationDeliveryTimeCommonInfo> allLocations =
                Arrays.asList(_1_to_2, _1_to_3, _2_to_3, _2_to_4, _3_to_5, _4_to_5, _5_to_6, _7_to_8, _9_to_8, _9_to_7);

        List<FoundLocationDeliveryTime> expected =
                Arrays.asList(
                        FoundLocationDeliveryTime.builder()
                                .locations(Arrays.asList(_1_to_2, _2_to_3, _3_to_5, _5_to_6))
                                .fullDeliveryTimeInMinutes(500)
                                .build(),
                        FoundLocationDeliveryTime.builder()
                                .locations(Arrays.asList(_1_to_2, _2_to_4, _4_to_5, _5_to_6))
                                .fullDeliveryTimeInMinutes(500)
                                .build(),
                        FoundLocationDeliveryTime.builder()
                                .locations(Arrays.asList(_1_to_3, _3_to_5, _5_to_6))
                                .fullDeliveryTimeInMinutes(500)
                                .build());

        // when
        LocationDeliveryTimeSearcher calculator = new LocationDeliveryTimeSearcher(allLocations, 1, 6);
        List<FoundLocationDeliveryTime> actual = calculator.search();

        // then
        assertEquals(expected, actual);
    }

    @Test
    void search_From_1_to_2_found_2() {
        // given
        // First group of connected locations
        LocationDeliveryTimeCommonInfo _1_to_2 = LocationDeliveryTimeCommonInfo.builder().start(1).finish(2).timeInMinutes(400).build();
        LocationDeliveryTimeCommonInfo _1_to_3 = LocationDeliveryTimeCommonInfo.builder().start(1).finish(3).timeInMinutes(200).build();
        LocationDeliveryTimeCommonInfo _2_to_3 = LocationDeliveryTimeCommonInfo.builder().start(2).finish(3).timeInMinutes(200).build();
        LocationDeliveryTimeCommonInfo _2_to_4 = LocationDeliveryTimeCommonInfo.builder().start(2).finish(4).timeInMinutes(200).build();
        LocationDeliveryTimeCommonInfo _3_to_5 = LocationDeliveryTimeCommonInfo.builder().start(3).finish(5).timeInMinutes(200).build();
        LocationDeliveryTimeCommonInfo _4_to_5 = LocationDeliveryTimeCommonInfo.builder().start(4).finish(5).timeInMinutes(100).build();
        LocationDeliveryTimeCommonInfo _5_to_6 = LocationDeliveryTimeCommonInfo.builder().start(5).finish(6).timeInMinutes(100).build();

        // Second group of connected locations. Two groups are not connected between each other!
        LocationDeliveryTimeCommonInfo _7_to_8 = LocationDeliveryTimeCommonInfo.builder().start(7).finish(8).timeInMinutes(500).build();
        LocationDeliveryTimeCommonInfo _9_to_8 = LocationDeliveryTimeCommonInfo.builder().start(9).finish(8).timeInMinutes(500).build();
        LocationDeliveryTimeCommonInfo _9_to_7 = LocationDeliveryTimeCommonInfo.builder().start(9).finish(7).timeInMinutes(500).build();

        List<LocationDeliveryTimeCommonInfo> allLocations =
                Arrays.asList(_1_to_2, _1_to_3, _2_to_3, _2_to_4, _3_to_5, _4_to_5, _5_to_6, _7_to_8, _9_to_8, _9_to_7);

        List<FoundLocationDeliveryTime> expected =
                Arrays.asList(
                        FoundLocationDeliveryTime.builder()
                                .locations(Collections.singletonList(_1_to_2))
                                .fullDeliveryTimeInMinutes(400)
                                .build(),
                        FoundLocationDeliveryTime.builder()
                                .locations(Arrays.asList(_1_to_3, _2_to_3))
                                .fullDeliveryTimeInMinutes(400)
                                .build());


        // when
        LocationDeliveryTimeSearcher calculator = new LocationDeliveryTimeSearcher(allLocations, 1, 2);
        List<FoundLocationDeliveryTime> actual = calculator.search();

        // then
        assertEquals(expected, actual);
    }

    @Test
    void search_From_1_to_8_empty() {
        // given
        // First group of connected locations
        LocationDeliveryTimeCommonInfo _1_to_2 = LocationDeliveryTimeCommonInfo.builder().start(1).finish(2).timeInMinutes(200).build();
        LocationDeliveryTimeCommonInfo _1_to_3 = LocationDeliveryTimeCommonInfo.builder().start(1).finish(3).timeInMinutes(200).build();
        LocationDeliveryTimeCommonInfo _2_to_3 = LocationDeliveryTimeCommonInfo.builder().start(2).finish(3).timeInMinutes(200).build();
        LocationDeliveryTimeCommonInfo _2_to_4 = LocationDeliveryTimeCommonInfo.builder().start(2).finish(4).timeInMinutes(200).build();
        LocationDeliveryTimeCommonInfo _3_to_5 = LocationDeliveryTimeCommonInfo.builder().start(3).finish(5).timeInMinutes(200).build();
        LocationDeliveryTimeCommonInfo _4_to_5 = LocationDeliveryTimeCommonInfo.builder().start(4).finish(5).timeInMinutes(100).build();
        LocationDeliveryTimeCommonInfo _5_to_6 = LocationDeliveryTimeCommonInfo.builder().start(5).finish(6).timeInMinutes(100).build();

        // Second group of connected locations. Two groups are not connected between each other!
        LocationDeliveryTimeCommonInfo _7_to_8 = LocationDeliveryTimeCommonInfo.builder().start(7).finish(8).timeInMinutes(500).build();
        LocationDeliveryTimeCommonInfo _9_to_8 = LocationDeliveryTimeCommonInfo.builder().start(9).finish(8).timeInMinutes(500).build();
        LocationDeliveryTimeCommonInfo _9_to_7 = LocationDeliveryTimeCommonInfo.builder().start(9).finish(7).timeInMinutes(500).build();

        List<LocationDeliveryTimeCommonInfo> allLocations =
                Arrays.asList(_1_to_2, _1_to_3, _2_to_3, _2_to_4, _3_to_5, _4_to_5, _5_to_6, _7_to_8, _9_to_8, _9_to_7);

        // when
        LocationDeliveryTimeSearcher calculator = new LocationDeliveryTimeSearcher(allLocations, 1, 8);
        List<FoundLocationDeliveryTime> actual = calculator.search();

        // then
        assertTrue(actual.isEmpty());
    }

}