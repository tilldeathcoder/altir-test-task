package com.altir.service.warehouse;

import com.altir.persistence.location.entity.*;
import com.altir.persistence.warehouse.*;
import com.altir.persistence.warehouse.entity.*;
import com.altir.service.exception.*;
import com.altir.service.location.cache.*;
import com.altir.service.location.model.*;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.*;
import org.mockito.*;
import org.mockito.junit.jupiter.*;

import java.util.*;

import static java.util.Arrays.*;
import static java.util.Collections.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class WarehouseServiceTest {

    @Mock
    private WarehouseRepository warehouseRepository;

    @Mock
    private WarehousesToCustomersRepository warehousesToCustomersRepository;

    @Mock
    private LocationDeliveryTimeInMemoryCache locationDeliveryTimeInMemoryCache;

    @InjectMocks
    private WarehouseService warehouseService;

    @Test
    void findByIdOrThrowException_existingId_ok() {
        // given
        Warehouse expected = Warehouse.builder().id(1).title("Test").build();

        // when
        when(warehouseRepository.findById(1L)).thenReturn(Optional.of(expected));
        Warehouse actual = warehouseService.findByIdOrThrowException(1);

        // then
        assertEquals(expected, actual);
    }

    @Test
    void findByIdOrThrowException_notExistingId_exception() {
        // when
        when(warehouseRepository.findById(1L)).thenReturn(Optional.empty());

        // then
        assertThrows(ResourceNotFoundException.class, () -> warehouseService.findByIdOrThrowException(1));
    }

    @Test
    void findNearestWarehouseForCustomerOrThrowException_ok() {
        // given
        WarehousesToCustomers expected = WarehousesToCustomers.builder()
                .id(WarehousesToCustomersCompositeKey.builder()
                        .customerId(1)
                        .warehouseId(1)
                        .build())
                .fullDeliveryTimeInMinutes(-1)
                .build();

        List<WarehousesToCustomers> queryResult = asList(expected,
                WarehousesToCustomers.builder()
                        .id(WarehousesToCustomersCompositeKey.builder()
                                .customerId(1)
                                .warehouseId(2)
                                .build())
                        .fullDeliveryTimeInMinutes(200)
                        .build()
                );

        List<Long> ids = asList(1L,2L,3L);

        // when
        when(warehousesToCustomersRepository.findByIdsAndCustomerId(ids, 1)).thenReturn(queryResult);

        WarehousesToCustomers actual = warehouseService.findNearestWarehouseForCustomerOrThrowException(ids, 1);

        // then
        assertEquals(expected, actual);
        verify(warehousesToCustomersRepository, times(1)).findByIdsAndCustomerId(ids, 1);
    }

    @Test
    void findNearestWarehouseForCustomerOrThrowException_exception() {
        // given
        List<Long> ids = asList(1L,2L,3L);

        // when
        when(warehousesToCustomersRepository.findByIdsAndCustomerId(ids, 1)).thenReturn(emptyList());

        // then
        assertThrows(ProductDeliveryException.class, () -> warehouseService.findNearestWarehouseForCustomerOrThrowException(ids, 1));
    }

    @Test
    void addWarehousesToCustomer_oneWarehouseWithSameLocationAsCustomer_ok() {
        // given
        Warehouse warehouse1 = Warehouse.builder()
                .id(1)
                .location(Location.builder()
                        .id(1)
                        .build())
                .build();
        Warehouse warehouse2 = Warehouse.builder()
                .id(2)
                .location(Location.builder()
                        .id(2)
                        .build())
                .build();
        Warehouse warehouse3 = Warehouse.builder()
                .id(3)
                .location(Location.builder()
                        .id(3)
                        .build())
                .build();

        List<Warehouse> warehouses = asList(warehouse1, warehouse2, warehouse3);

        FoundLocationDeliveryTime foundLocationDeliveryTime2 = FoundLocationDeliveryTime.builder()
                .fullDeliveryTimeInMinutes(100)
                .build();

        // when
        when(warehouseRepository.findAll()).thenReturn(warehouses);
        when(locationDeliveryTimeInMemoryCache.get(2, 1)).thenReturn(emptyList());
        when(locationDeliveryTimeInMemoryCache.get(2, 3)).thenReturn(singletonList(foundLocationDeliveryTime2));

        WarehousesToCustomers warehousesToCustomers2 = WarehousesToCustomers.builder()
                .id(WarehousesToCustomersCompositeKey.builder()
                        .warehouseId(2)
                        .customerId(1)
                        .build())
                .fullDeliveryTimeInMinutes(-1)
                .build();
        WarehousesToCustomers warehousesToCustomers3 = WarehousesToCustomers.builder()
                .id(WarehousesToCustomersCompositeKey.builder()
                        .warehouseId(3)
                        .customerId(1)
                        .build())
                .fullDeliveryTimeInMinutes(100)
                .build();

        warehouseService.addWarehousesToCustomer(1, 2);
        // then
        verify(warehousesToCustomersRepository, times(1)).saveAll(asList(warehousesToCustomers2, warehousesToCustomers3));
    }

}