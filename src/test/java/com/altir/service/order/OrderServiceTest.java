package com.altir.service.order;

import com.altir.persistence.customer.entity.*;
import com.altir.persistence.location.entity.*;
import com.altir.persistence.order.*;
import com.altir.persistence.order.entity.Order;
import com.altir.persistence.product.entity.*;
import com.altir.persistence.warehouse.entity.*;
import com.altir.service.customer.*;
import com.altir.service.exception.*;
import com.altir.service.order.model.*;
import com.altir.service.product.*;
import com.altir.service.warehouse.*;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.*;
import org.mockito.*;
import org.mockito.junit.jupiter.*;

import java.time.*;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class OrderServiceTest {
    @Mock
    private OrderRepository orderRepository;
    @Mock
    private CustomerService customerService;
    @Mock
    private ProductService productService;
    @Mock
    private WarehouseService warehouseService;
    @InjectMocks
    private OrderService orderService;

    @Test
    void create_notExistingCustomer_exception() {
        // given
        OrderCreateRequest request = OrderCreateRequest.builder()
                .customerId(1)
                .productId(11)
                .build();

        // when
        when(customerService.findByIdOrThrowException(1)).thenThrow(ResourceNotFoundException.class);

        // then
        assertThrows(ResourceNotFoundException.class, () -> orderService.create(request));
    }

    @Test
    void create_notExistingProduct_exception() {
        // given
        OrderCreateRequest request = OrderCreateRequest.builder()
                .customerId(1)
                .productId(11)
                .build();

        Customer customer = Customer.builder().build();

        // when
        when(customerService.findByIdOrThrowException(1)).thenReturn(customer);
        when(productService.findByIdOrThrowException(11)).thenThrow(ResourceNotFoundException.class);

        // then
        assertThrows(ResourceNotFoundException.class, () -> orderService.create(request));
        verify(customerService, times(1)).findByIdOrThrowException(1);
    }

    @Test
    void create_productIsOutOfStock_exception() {
        // given
        OrderCreateRequest request = OrderCreateRequest.builder()
                .customerId(1)
                .productId(11)
                .build();

        Customer customer = Customer.builder().build();
        Product product = Product.builder().build();

        // when
        when(customerService.findByIdOrThrowException(1)).thenReturn(customer);
        when(productService.findByIdOrThrowException(11)).thenReturn(product);
        when(productService.findAllAvailableByProductIdOrThrowException(11, 1)).thenThrow(NoAvailableProductInWarehousesException.class);

        // then
        assertThrows(NoAvailableProductInWarehousesException.class, () -> orderService.create(request));
        verify(customerService, times(1)).findByIdOrThrowException(1);
        verify(productService, times(1)).findByIdOrThrowException(11);
    }

    @Test
    void create_ok() {
        // given
        OrderCreateRequest request = OrderCreateRequest.builder()
                .customerId(1)
                .productId(11)
                .build();

        Location location = Location.builder()
                .id(1)
                .build();

        Customer customer = Customer.builder()
                .location(location)
                .build();
        Product product = Product.builder().build();
        ProductCountInWarehouse productCount = ProductCountInWarehouse.builder()
                .id(ProductsCountInWarehouseCompositeKey.builder()
                        .warehouseId(111)
                        .productId(11)
                        .build())
                .count(1)
                .build();

        WarehousesToCustomers warehouse = WarehousesToCustomers.builder()
                .id(WarehousesToCustomersCompositeKey.builder()
                        .warehouseId(111)
                        .customerId(1)
                        .build())
                .fullDeliveryTimeInMinutes(-1)
                .build();

        Order order = Order.builder()
                .created(LocalDateTime.now())
                .id(1)
                .customerId(1)
                .productId(11)
                .build();

        Warehouse warehouseFromDb = Warehouse.builder()
                .id(1)
                .location(location)
                .build();

        // when
        when(customerService.findByIdOrThrowException(1)).thenReturn(customer);
        when(productService.findByIdOrThrowException(11)).thenReturn(product);
        when(productService.findAllAvailableByProductIdOrThrowException(11, 1)).thenReturn(Collections.singletonList(productCount));
        when(warehouseService.findNearestWarehouseForCustomerOrThrowException(Collections.singletonList(111L), 1)).thenReturn(warehouse);
        when(warehouseService.findByIdOrThrowException(111)).thenReturn(warehouseFromDb);
        when(orderRepository.save(any())).thenReturn(order);
        doNothing().when(productService).updateProductCountInWarehouse(Collections.singletonList(productCount), 111);

        orderService.create(request);

        // then
        verify(customerService, times(1)).findByIdOrThrowException(1);
        verify(productService, times(1)).findByIdOrThrowException(11);
        verify(productService, times(1)).findAllAvailableByProductIdOrThrowException(11, 1);
        verify(warehouseService, times(1)).findNearestWarehouseForCustomerOrThrowException(Collections.singletonList(111L), 1);
        verify(orderRepository, times(1)).save(any());
        verify(productService, times(1)).updateProductCountInWarehouse(Collections.singletonList(productCount), 111);
    }
}