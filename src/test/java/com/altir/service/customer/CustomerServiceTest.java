package com.altir.service.customer;

import com.altir.persistence.customer.*;
import com.altir.persistence.customer.entity.*;
import com.altir.persistence.location.entity.*;
import com.altir.service.customer.model.*;
import com.altir.service.exception.*;
import com.altir.service.location.*;
import com.altir.service.warehouse.*;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.*;
import org.mockito.*;
import org.mockito.junit.jupiter.*;

import static com.altir.service.exception.ResourceNotFoundException.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class CustomerServiceTest {

    @Mock
    CustomerRepository customerRepository;

    @Mock
    LocationService locationService;

    @Mock
    WarehouseService warehouseService;

    @InjectMocks
    CustomerService customerService;

    @Test
    void create_validCustomer_ok() {
        // given
        Location location = Location.builder().build();
        Customer customerToRepository = Customer.builder()
                .name("Test")
                .location(location)
                .build();

        Customer databaseInsertResult = Customer.builder().id(2).location(location).build();

        CustomerCreateRequest request = CustomerCreateRequest.builder().name("Test").locationId(1).build();

        // when
        when(locationService.findByIdOrThrowException(1)).thenReturn(location);
        when(customerRepository.save(customerToRepository)).thenReturn(databaseInsertResult);
        doNothing().when(warehouseService).addWarehousesToCustomer(2, 1);

        customerService.create(request);

        // then
        verify(locationService, times(1)).findByIdOrThrowException(1);
        verify(customerRepository, times(1)).save(customerToRepository);
        verify(warehouseService, times(1)).addWarehousesToCustomer(2, 1);
    }

    @Test
    void create_customerWithNotExistLocation_exception() {
        // given
        CustomerCreateRequest request = CustomerCreateRequest.builder().name("Test").locationId(1).build();

        // when
        when(locationService.findByIdOrThrowException(1)).thenThrow(createLocationNotFoundException(1));

        // then
        assertThrows(ResourceNotFoundException.class, () -> customerService.create(request));
    }

}