-- Locations
---- First group of locations
insert into locations values (1, 'A_1');
insert into locations values (2, 'B_1');
insert into locations values (3, 'C_1');
insert into locations values (4, 'E_1');
insert into locations values (5, 'F_1');
insert into locations values (6, 'I_1');
------ A_1 - B_1
insert into locations_delivery_time values (1, 2, 30);
------ A_1 - E_1
insert into locations_delivery_time values (1, 4, 30);
------ B_1 - C_1
insert into locations_delivery_time values (2, 3, 40);
------ B_1 - E_1
insert into locations_delivery_time values (2, 4, 30);
------ F_1 - C_1
insert into locations_delivery_time values (5, 3, 30);
------ F_1 - E_1
insert into locations_delivery_time values (5, 4, 30);
------ F_1 - I_1
insert into locations_delivery_time values (5, 6, 30);
---- Second group of location. Second group and first group are not connected!
insert into locations values (7, 'D_2');
insert into locations values (8, 'H_2');
insert into locations values (9, 'G_2');
------ D_2 - H_2
insert into locations_delivery_time values (7, 8, 30);
------ H_2 - G_2
insert into locations_delivery_time values (8, 9, 40);
------ G_2 - D_2
insert into locations_delivery_time values (9, 7, 50);

-- Warehouses
---- First group locations
insert into warehouses values (1, 'Warehouse_A_1', 1);
insert into warehouses values (2, 'Warehouse_B_1', 2);
insert into warehouses values (3, 'Warehouse_C_1', 3);
insert into warehouses values (4, 'Warehouse_E_1', 4);
insert into warehouses values (5, 'Warehouse_F_1', 5);
insert into warehouses values (6, 'Warehouse_I_1', 6);
---- Second group locations
insert into warehouses values (7, 'Warehouse_D_2', 7);
insert into warehouses values (8, 'Warehouse_H_2', 8);
insert into warehouses values (9, 'Warehouse_G_2', 9);

-- Products
insert into products values (1, 'IPhone', 'Model: 13', 10.0);
insert into products values (2, 'Samsung', 'Model: Galaxy', 100.0);
insert into products values (3, 'Nokia', 'Model: 3310', 1000.0);
insert into products values (4, 'Fake', 'This product is not available', 0.0);
---- Product count in warehouse
------ Product 1
insert into product_count_in_warehouse values (1, 1, 2);
insert into product_count_in_warehouse values (1, 2, 10);
insert into product_count_in_warehouse values (1, 3, 20);
insert into product_count_in_warehouse values (1, 4, 10);
insert into product_count_in_warehouse values (1, 5, 0);
insert into product_count_in_warehouse values (1, 6, 20);
insert into product_count_in_warehouse values (1, 7, 30);
insert into product_count_in_warehouse values (1, 8, 40);
insert into product_count_in_warehouse values (1, 9, 50);
------ Product 2
insert into product_count_in_warehouse values (2, 1, 10);
insert into product_count_in_warehouse values (2, 2, 100);
insert into product_count_in_warehouse values (2, 3, 0);
insert into product_count_in_warehouse values (2, 4, 0);
insert into product_count_in_warehouse values (2, 5, 20);
insert into product_count_in_warehouse values (2, 6, 0);
insert into product_count_in_warehouse values (2, 7, 330);
insert into product_count_in_warehouse values (2, 8, 0);
insert into product_count_in_warehouse values (2, 9, 50);
------ Product 3
insert into product_count_in_warehouse values (3, 1, 20);
insert into product_count_in_warehouse values (3, 2, 10);
insert into product_count_in_warehouse values (3, 3, 20);
insert into product_count_in_warehouse values (3, 4, 0);
insert into product_count_in_warehouse values (3, 5, 0);
insert into product_count_in_warehouse values (3, 6, 20);
insert into product_count_in_warehouse values (3, 7, 30);
insert into product_count_in_warehouse values (3, 8, 40);
insert into product_count_in_warehouse values (3, 9, 0);