-- Drop tables
drop table if exists customers CASCADE;;
drop table if exists locations CASCADE;
drop table if exists locations_delivery_time CASCADE;
drop table if exists orders CASCADE;
drop table if exists products CASCADE;
drop table if exists product_count_in_warehouse CASCADE;
drop table if exists warehouses CASCADE;

-- Create tables
create table customers (id bigint not null, name varchar(255), location_id bigint, primary key (id));
create table locations (id bigint not null, name varchar(255), primary key (id));
create table locations_delivery_time (location_finish bigint not null, location_start bigint not null, time_in_minutes bigint not null, primary key (location_finish, location_start));
create table orders (id bigint not null, created timestamp, price double not null, customer_id bigint, product_id bigint, warehouse_id bigint, primary key (id));
create table products (id bigint not null, description varchar(255), title varchar(255), price double not null, primary key (id));
create table product_count_in_warehouse (product_id bigint not null, warehouse_id bigint not null, count bigint not null, primary key (product_id, warehouse_id));
create table warehouses (id bigint not null, title varchar(255), location_id bigint, primary key (id));
create table warehouses_to_customers (customer_id bigint not null, warehouse_id bigint not null, full_delivery_time_in_minutes bigint not null, primary key (customer_id, warehouse_id));

-- Add foreign keys
alter table customers add constraint customers_locations_fk foreign key (location_id) references locations;
alter table locations_delivery_time add constraint locations_delivery_time_locations_fk1 foreign key (location_finish) references locations;
alter table locations_delivery_time add constraint locations_delivery_time_locations_fk2 foreign key (location_start) references locations;
alter table orders add constraint orders_customers_fk foreign key (customer_id) references customers;
alter table orders add constraint orders_products_fk foreign key (product_id) references products;
alter table orders add constraint orders_warehouses_fk foreign key (warehouse_id) references warehouses;
alter table warehouses add constraint warehouses_locations_fk foreign key (location_id) references locations;