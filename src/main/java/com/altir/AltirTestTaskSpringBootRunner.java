package com.altir;


import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;

/**
 * The entry point of the application.
 *
 * @author Yauhen.Makaranka
 */
@SpringBootApplication
public class AltirTestTaskSpringBootRunner {

    public static void main(String[] args) {
        SpringApplication.run(AltirTestTaskSpringBootRunner.class, args);
    }
}
