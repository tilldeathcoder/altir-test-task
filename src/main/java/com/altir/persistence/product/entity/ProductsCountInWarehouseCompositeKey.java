package com.altir.persistence.product.entity;

import lombok.*;
import lombok.experimental.*;

import javax.persistence.*;
import java.io.*;

@Embeddable
@Data
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@EqualsAndHashCode
public class ProductsCountInWarehouseCompositeKey implements Serializable {

    @Column(name = "product_id")
    private long productId;
    @Column(name = "warehouse_id")
    private long warehouseId;

}
