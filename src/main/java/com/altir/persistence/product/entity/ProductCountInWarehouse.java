package com.altir.persistence.product.entity;

import lombok.*;
import lombok.experimental.*;

import javax.persistence.*;

@Entity
@Table(name = "product_count_in_warehouse")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@EqualsAndHashCode
public class ProductCountInWarehouse {

    @EmbeddedId private ProductsCountInWarehouseCompositeKey id;
    private long count;

}
