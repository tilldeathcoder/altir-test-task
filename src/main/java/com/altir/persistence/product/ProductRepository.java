package com.altir.persistence.product;

import com.altir.persistence.product.entity.*;
import org.springframework.data.repository.*;

public interface ProductRepository extends CrudRepository<Product, Long> {
}
