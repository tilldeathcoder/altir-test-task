package com.altir.persistence.product;

import com.altir.persistence.product.entity.*;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.*;

import java.util.*;

public interface ProductCountInWarehouseRepository extends CrudRepository<ProductCountInWarehouse, ProductsCountInWarehouseCompositeKey> {

    @Query("select p from ProductCountInWarehouse p " +
            "where p.id.productId = :productId " +
            "and p.count > 0 " +
            "and p.id.warehouseId in (select w.id.warehouseId from WarehousesToCustomers w where w.id.customerId = :customerId) " +
            "order by p.count desc")
    List<ProductCountInWarehouse> findAllAvailableByProductId(long productId, long customerId);

}
