package com.altir.persistence.location;

import com.altir.persistence.location.entity.*;
import org.springframework.data.repository.*;

import java.util.*;

public interface LocationDeliveryTimeRepository extends CrudRepository<LocationsDeliveryTime, DeliveryTimeCompositeKey> {

    List<LocationsDeliveryTime> findAll();

}
