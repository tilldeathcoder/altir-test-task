package com.altir.persistence.location.entity;

import lombok.*;

import javax.persistence.*;
import java.io.*;

@Embeddable
@Data
@NoArgsConstructor
@AllArgsConstructor
public class DeliveryTimeCompositeKey implements Serializable {

    @Column(name = "location_start") private long locationStart;
    @Column(name = "location_finish") private long locationFinish;

}
