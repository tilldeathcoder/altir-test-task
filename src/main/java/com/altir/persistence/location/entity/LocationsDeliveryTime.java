package com.altir.persistence.location.entity;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "locations_delivery_time")
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class LocationsDeliveryTime {

    @EmbeddedId private DeliveryTimeCompositeKey id;

    @ManyToOne
    @MapsId("id")
    @JoinColumn(name = "location_start")
    private Location locationStart;

    @ManyToOne
    @MapsId("id")
    @JoinColumn(name = "location_finish")
    private Location locationFinish;

    @Column(name = "time_in_minutes")
    private long timeInMinutes;

}
