package com.altir.persistence.location;

import com.altir.persistence.location.entity.*;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.*;

import java.util.*;

public interface LocationRepository extends CrudRepository<Location, Long> {

    @Query("select l from Location l where l.id in (select w.location.id from Warehouse w where w.id in :ids)")
    List<Location> findAllByWarehouseIds(List<Long> ids);

    List<Location> findAll();

}
