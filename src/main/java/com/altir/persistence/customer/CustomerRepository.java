package com.altir.persistence.customer;

import com.altir.persistence.customer.entity.*;
import org.springframework.data.repository.*;

public interface CustomerRepository extends CrudRepository<Customer, Long> { }
