package com.altir.persistence.customer.entity;

import com.altir.persistence.location.entity.*;
import com.altir.persistence.warehouse.entity.*;
import lombok.*;
import lombok.experimental.*;
import org.hibernate.annotations.*;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.*;
import java.util.*;

@Entity
@Table(name = "customers")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@SuperBuilder
public class Customer {

    @Id
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "increment")
    private long id;
    private String name;

    @ManyToOne
    private Location location;

    @ManyToMany
    private Set<WarehousesToCustomers> warehousesToCustomers;

}
