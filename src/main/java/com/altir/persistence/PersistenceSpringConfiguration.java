package com.altir.persistence;

import org.springframework.context.annotation.*;
import org.springframework.data.jpa.repository.config.*;
import org.springframework.transaction.annotation.*;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories("com.altir.persistence")
public class PersistenceSpringConfiguration {
}
