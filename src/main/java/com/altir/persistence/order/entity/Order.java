package com.altir.persistence.order.entity;

import lombok.*;
import lombok.experimental.*;
import org.hibernate.annotations.*;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.*;
import java.time.*;

@Entity
@Table(name = "orders")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
public class Order {

    @Id
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "increment")
    private long id;

    @Column(name = "product_id")
    private long productId;
    @Column(name = "customer_id")
    private long customerId;
    @Column(name = "warehouse_id")
    private long warehouseId;

    private double price;
    private LocalDateTime created;
}
