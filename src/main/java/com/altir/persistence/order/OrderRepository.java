package com.altir.persistence.order;

import com.altir.persistence.order.entity.*;
import org.springframework.data.repository.*;

public interface OrderRepository extends CrudRepository<Order, Long> {
}
