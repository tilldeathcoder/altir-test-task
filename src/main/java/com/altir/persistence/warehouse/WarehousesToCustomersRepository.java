package com.altir.persistence.warehouse;

import com.altir.persistence.warehouse.entity.*;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.*;

import java.util.*;

public interface WarehousesToCustomersRepository extends CrudRepository<WarehousesToCustomers, WarehousesToCustomersCompositeKey> {

    @Query("select wc from WarehousesToCustomers wc where wc.id.warehouseId in :ids and wc.id.customerId = :customerId order by wc.fullDeliveryTimeInMinutes")
    List<WarehousesToCustomers> findByIdsAndCustomerId(List<Long> ids, long customerId);

}
