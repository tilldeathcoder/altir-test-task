package com.altir.persistence.warehouse;

import com.altir.persistence.warehouse.entity.*;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.*;

import java.util.*;

public interface WarehouseRepository extends CrudRepository<Warehouse, Long> {

    List<Warehouse> findAll();

    @Query("select w from Warehouse w where w.id in (select wc.id.warehouseId from WarehousesToCustomers wc where wc.id in :ids and wc.id.customerId = :customerId)")
    Optional<Warehouse> findByIdsAndCustomerIdNearest(List<Long>ids, long customerId);

}
