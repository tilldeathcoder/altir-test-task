package com.altir.persistence.warehouse.entity;

import lombok.*;
import lombok.experimental.*;

import javax.persistence.*;
import java.io.*;

@Embeddable
@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class WarehousesToCustomersCompositeKey implements Serializable {

    @Column(name = "customer_id")
    private long customerId;
    @Column(name = "warehouse_id")
    private long warehouseId;
    
}
