package com.altir.persistence.warehouse.entity;

import lombok.*;
import lombok.experimental.*;

import javax.persistence.*;

@Entity
@Table(name = "warehouses_to_customers")
@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class WarehousesToCustomers {

    @EmbeddedId private WarehousesToCustomersCompositeKey id;
    @Column(name = "full_delivery_time_in_minutes")
    private long fullDeliveryTimeInMinutes;
}
