package com.altir.web.v1.rest.controller;

import com.altir.service.order.*;
import com.altir.service.order.model.*;
import lombok.*;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import javax.validation.*;

import static org.springframework.http.HttpStatus.*;

@RestController
@RequestMapping(value = "/v1/rest/orders", produces = {"application/json"}, consumes = {"application/json"})
@RequiredArgsConstructor
public class OrderRestController {

    private final OrderService orderService;

    @PostMapping
    public ResponseEntity<CreatedOrderResponse> create(@RequestBody @Valid OrderCreateRequest request) {
        return new ResponseEntity<>(orderService.create(request), CREATED);
    }

}
