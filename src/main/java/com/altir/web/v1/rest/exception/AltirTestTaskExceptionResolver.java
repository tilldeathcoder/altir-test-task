package com.altir.web.v1.rest.exception;

import com.altir.service.exception.*;
import lombok.extern.slf4j.*;
import org.apache.commons.lang3.*;
import org.springframework.http.*;
import org.springframework.validation.*;
import org.springframework.web.bind.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.*;
import org.springframework.web.servlet.mvc.method.annotation.*;

import java.util.*;

import static java.util.Map.*;
import static java.util.Objects.*;
import static org.hibernate.internal.util.collections.CollectionHelper.*;
import static org.springframework.http.HttpStatus.*;

@ControllerAdvice
@Slf4j
public class AltirTestTaskExceptionResolver extends ResponseEntityExceptionHandler {

  @ExceptionHandler(ResourceNotFoundException.class)
  public ResponseEntity<Map<String, Object>> handleResourceNotFoundException(ResourceNotFoundException exception) {
    log.error(exception.getMessage(), exception);
    return new ResponseEntity<>(of("message", exception.getMessage(), "parameters", of("resource", exception.getResource(), "qualifier", exception.getQualifier())),NOT_FOUND);
  }

  @ExceptionHandler(NoAvailableProductInWarehousesException.class)
  public ResponseEntity<Map<String, Object>> handleNoAvailableProductInWarehousesException(NoAvailableProductInWarehousesException exception) {
    log.error(exception.getMessage(), exception);
    return new ResponseEntity<>(of("message", exception.getMessage(), "parameters", of("productId", exception.getProductId())),CONFLICT);
  }

  @ExceptionHandler(ProductDeliveryException.class)
  public ResponseEntity<Map<String, Object>> handleProductDeliveryException(ProductDeliveryException exception) {
    log.error(exception.getMessage(), exception);
    return new ResponseEntity<>(of("message", exception.getMessage(), "parameters", of("customerId", exception.getCustomerId())),CONFLICT);
  }

  @Override
  protected ResponseEntity<Object> handleMethodArgumentNotValid(
          MethodArgumentNotValidException exception,
          HttpHeaders headers,
          HttpStatus status,
          WebRequest request) {
    log.error(exception.getMessage(), exception);
    List<FieldError> errors = exception.getBindingResult().getFieldErrors();
    if (isNotEmpty(errors)) {
      List<Map<String, Object>> fieldErrors = new ArrayList<>();
      errors.forEach(error -> {
        Map<String, Object> parameters = new HashMap<>();
        if (StringUtils.isNotEmpty(error.getField())){
          parameters.put("field", error.getField());
        }
        if (StringUtils.isNotEmpty(error.getDefaultMessage())){
          parameters.put("message", error.getDefaultMessage());
        }
        if (nonNull(error.getRejectedValue())){
          parameters.put("rejectedValue", error.getRejectedValue());
        }
        fieldErrors.add(parameters);
      });

      return new ResponseEntity<>(of("message", "request body is invalid", "parameters", of("errors", fieldErrors)), HttpStatus.BAD_REQUEST);
    }

    return new ResponseEntity<>(of("message", "request body is invalid"), HttpStatus.BAD_REQUEST);
  }

}
