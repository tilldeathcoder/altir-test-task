package com.altir.web.v1.rest.controller;

import com.altir.service.customer.*;
import com.altir.service.customer.model.*;
import lombok.*;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import javax.validation.*;

import static org.springframework.http.HttpStatus.*;

@RestController
@RequestMapping(value = "/v1/rest/customers", produces = {"application/json"}, consumes = {"application/json"})
@RequiredArgsConstructor
public class CustomerRestController {

    private final CustomerService customerService;

    @PostMapping
    public ResponseEntity<CreatedCustomerResponse> create(@RequestBody @Valid CustomerCreateRequest request) {
        return new ResponseEntity<>(customerService.create(request), CREATED);
    }

}
