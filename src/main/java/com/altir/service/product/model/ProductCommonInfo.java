package com.altir.service.product.model;

import com.altir.persistence.product.entity.*;
import lombok.*;
import lombok.experimental.*;

@Data
@SuperBuilder
public class ProductCommonInfo {

    private long id;
    private String title;
    private String description;

    public ProductCommonInfo(Product product) {
        id = product.getId();
        title = product.getTitle();
        description = product.getDescription();
    }
}
