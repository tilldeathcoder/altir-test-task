package com.altir.service.product;

import com.altir.persistence.product.*;
import com.altir.persistence.product.entity.*;
import com.altir.service.exception.*;
import lombok.*;
import org.springframework.stereotype.*;

import java.util.*;

import static com.altir.service.exception.ResourceNotFoundException.*;
import static org.apache.commons.lang3.ObjectUtils.*;

@Service
@RequiredArgsConstructor
public class ProductService {

    private final ProductRepository productRepository;
    private final ProductCountInWarehouseRepository productCountInWarehouseRepository;

    public Product findByIdOrThrowException(long id) {
        return productRepository.findById(id).orElseThrow(() -> createProductNotFoundException(id));
    }

    public List<ProductCountInWarehouse> findAllAvailableByProductIdOrThrowException(long productId, long customerId) {
        List<ProductCountInWarehouse> productCountInWarehouses = productCountInWarehouseRepository.findAllAvailableByProductId(productId, customerId);
        if (isEmpty(productCountInWarehouses)) {
            throw new NoAvailableProductInWarehousesException(productId);
        }

        return productCountInWarehouses;
    }

    public void updateProductCountInWarehouse(List<ProductCountInWarehouse> productCountInWarehouseList, long selectedWarehouseId) {
        Optional<ProductCountInWarehouse> countInWarehouse = productCountInWarehouseList.stream().filter(productCountInWarehouse -> productCountInWarehouse.getId().getWarehouseId() == selectedWarehouseId).findAny();
        if (countInWarehouse.isPresent()) {
            ProductCountInWarehouse productCountInWarehouse = countInWarehouse.get();
            productCountInWarehouse.setCount(productCountInWarehouse.getCount() - 1);
            productCountInWarehouseRepository.save(productCountInWarehouse);
        }

    }

}
