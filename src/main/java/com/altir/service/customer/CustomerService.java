package com.altir.service.customer;

import com.altir.persistence.customer.*;
import com.altir.persistence.customer.entity.*;
import com.altir.service.customer.model.*;
import com.altir.service.location.*;
import com.altir.service.warehouse.*;
import lombok.*;
import org.springframework.stereotype.*;

import javax.transaction.*;

import static com.altir.service.exception.ResourceNotFoundException.*;

@Service
@RequiredArgsConstructor
public class CustomerService {

    private final CustomerRepository customerRepository;
    private final LocationService locationService;
    private final WarehouseService warehouseService;

    @Transactional
    public CreatedCustomerResponse create(CustomerCreateRequest request) {
        Customer created = customerRepository.save(Customer.builder()
                .name(request.getName())
                .location(locationService.findByIdOrThrowException(request.getLocationId()))
                .build());
        warehouseService.addWarehousesToCustomer(created.getId(), request.getLocationId());
        return new CreatedCustomerResponse(created);
    }

    public Customer findByIdOrThrowException(long id) {
        return customerRepository.findById(id).orElseThrow(() -> createCustomerNotFoundException(id));
    }

}
