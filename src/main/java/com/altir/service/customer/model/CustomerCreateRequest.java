package com.altir.service.customer.model;

import lombok.*;
import lombok.experimental.*;

import javax.validation.constraints.*;


@Data
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
public class CustomerCreateRequest {

    @NotEmpty(message = "filed can not be empty")
    private String name;
    @Positive(message = "filed must be positive")
    private long locationId;

}
