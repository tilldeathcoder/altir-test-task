package com.altir.service.customer.model;

import com.altir.persistence.customer.entity.*;
import com.altir.service.location.model.*;
import lombok.*;
import lombok.experimental.*;

@Data
@SuperBuilder
public class CustomerCommonInfo {

    private long id;
    private String name;
    private LocationCommonInfo location;

    public CustomerCommonInfo(Customer customer) {
        id = customer.getId();
        name = customer.getName();;
        location = new LocationCommonInfo(customer.getLocation());
    }
}
