package com.altir.service.order;

import com.altir.persistence.customer.entity.*;
import com.altir.persistence.order.*;
import com.altir.persistence.order.entity.*;
import com.altir.persistence.product.entity.*;
import com.altir.persistence.warehouse.entity.*;
import com.altir.service.customer.*;
import com.altir.service.customer.model.*;
import com.altir.service.order.model.*;
import com.altir.service.product.*;
import com.altir.service.product.model.*;
import com.altir.service.warehouse.*;
import com.altir.service.warehouse.model.*;
import lombok.*;
import org.springframework.stereotype.*;

import javax.transaction.*;
import java.time.*;
import java.util.*;

@Service
@RequiredArgsConstructor
public class OrderService {

    private final OrderRepository orderRepository;
    private final CustomerService customerService;
    private final ProductService productService;
    private final WarehouseService warehouseService;

    @Transactional
    public CreatedOrderResponse create(OrderCreateRequest request) {
        Customer customer = customerService.findByIdOrThrowException(request.getCustomerId());
        Product product = productService.findByIdOrThrowException(request.getProductId());
        List<ProductCountInWarehouse> productCountInWarehouses = productService.findAllAvailableByProductIdOrThrowException(request.getProductId(), request.getCustomerId());
        List<Long> warehousesIds = productCountInWarehouses.stream().map(productCountInWarehouse -> productCountInWarehouse.getId().getWarehouseId()).toList();
        WarehousesToCustomers warehouse = warehouseService.findNearestWarehouseForCustomerOrThrowException(warehousesIds, request.getCustomerId());

        Order order = orderRepository.save(Order.builder()
                .customerId(customer.getId())
                .productId(product.getId())
                .warehouseId(warehouse.getId().getWarehouseId())
                .price(product.getPrice())
                .created(LocalDateTime.now())
                .build());
        productService.updateProductCountInWarehouse(productCountInWarehouses, warehouse.getId().getWarehouseId());

        return CreatedOrderResponse.builder()
                .id(order.getId())
                .created(order.getCreated())
                .price(order.getPrice())
                .product(new ProductCommonInfo(product))
                .warehouse(new WarehouseCommonInfo(warehouseService.findByIdOrThrowException(warehouse.getId().getWarehouseId())))
                .customer(new CustomerCommonInfo(customer))
                .build();
    }
}
