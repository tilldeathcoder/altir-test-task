package com.altir.service.order.model;

import lombok.*;
import lombok.experimental.*;

import javax.validation.constraints.*;

@Data
@AllArgsConstructor
@SuperBuilder
public class OrderCreateRequest {

    @Positive(message = "filed must be positive")
    private long customerId;
    @Positive(message = "filed must be positive")
    private long productId;

}
