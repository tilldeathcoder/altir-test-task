package com.altir.service.order.model;

import com.altir.service.customer.model.*;
import com.altir.service.product.model.*;
import com.altir.service.warehouse.model.*;
import lombok.*;
import lombok.experimental.*;

import java.time.*;

@Data
@AllArgsConstructor
@SuperBuilder
public class CreatedOrderResponse {

    private long id;
    private CustomerCommonInfo customer;
    private WarehouseCommonInfo warehouse;
    private ProductCommonInfo product;
    private double price;
    private LocalDateTime created;
}
