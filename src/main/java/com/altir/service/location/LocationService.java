package com.altir.service.location;

import com.altir.persistence.location.*;
import com.altir.persistence.location.entity.*;
import lombok.*;
import org.springframework.stereotype.*;

import static com.altir.service.exception.ResourceNotFoundException.*;

@Service
@RequiredArgsConstructor
public class LocationService {

    private final LocationRepository locationRepository;

    public Location findByIdOrThrowException(long id) {
        return locationRepository.findById(id).orElseThrow(() -> createLocationNotFoundException(id));
    }
}
