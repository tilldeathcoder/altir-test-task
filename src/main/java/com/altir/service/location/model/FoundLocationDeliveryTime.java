package com.altir.service.location.model;

import lombok.*;
import lombok.experimental.*;

import java.util.*;

@Data
@SuperBuilder
public class FoundLocationDeliveryTime {

    private List<LocationDeliveryTimeCommonInfo> locations;
    private long fullDeliveryTimeInMinutes;

}
