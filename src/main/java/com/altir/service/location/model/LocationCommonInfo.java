package com.altir.service.location.model;

import com.altir.persistence.location.entity.*;
import lombok.*;
import lombok.experimental.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
public class LocationCommonInfo {

    private long id;
    private String name;

    public LocationCommonInfo(Location location) {
        id = location.getId();
        name = location.getName();
    }

}
