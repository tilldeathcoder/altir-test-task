package com.altir.service.location.model;

import com.altir.persistence.location.entity.*;
import lombok.*;
import lombok.experimental.*;

@Data
@SuperBuilder
public class LocationDeliveryTimeCommonInfo {

    private long start;
    private long finish;
    private long timeInMinutes;

    public LocationDeliveryTimeCommonInfo(LocationsDeliveryTime locationsDeliveryTime) {
        start = locationsDeliveryTime.getLocationStart().getId();
        finish = locationsDeliveryTime.getLocationFinish().getId();
        timeInMinutes = locationsDeliveryTime.getTimeInMinutes();
    }
}
