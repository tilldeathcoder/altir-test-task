package com.altir.service.location;

import com.altir.service.location.model.*;

import java.util.*;

public class LocationDeliveryTimeSearcher {

    private final List<LocationDeliveryTimeCommonInfo> locationsDeliveryTime;
    private final long start;
    private final long finish;
    private List<FoundLocationDeliveryTime> result;
    private long minTimeInMinutes = -1;

    public LocationDeliveryTimeSearcher(List<LocationDeliveryTimeCommonInfo> locationsDeliveryTime, long start, long finish) {
        this.locationsDeliveryTime = locationsDeliveryTime;
        this.start = start;
        this.finish = finish;
        this.result = new ArrayList<>();
    }

    public List<FoundLocationDeliveryTime> search() {
        List<LocationDeliveryTimeCommonInfo> dtos = getConnectedNotCheckedLocations(start, Collections.emptyList());
        List<Long> checkedLocations = new ArrayList<>();
        checkedLocations.add(start);
        dtos.forEach(
                deliveryTime ->
                        findDeliveryTime(
                                Collections.singletonList(deliveryTime),
                                getNextLocationToCheck(deliveryTime, start),
                                checkedLocations));

        return result;
    }

    private List<LocationDeliveryTimeCommonInfo> getConnectedNotCheckedLocations(long id, List<Long> checkedLocations) {
        return locationsDeliveryTime.stream()
                .filter(
                        locationDeliveryTimeCommonInfo ->
                                (locationDeliveryTimeCommonInfo.getStart() == id
                                        || locationDeliveryTimeCommonInfo.getFinish() == id)
                                        && isNotChecked(checkedLocations, locationDeliveryTimeCommonInfo))
                .toList();
    }

    private boolean isNotChecked(List<Long> checkedLocations, LocationDeliveryTimeCommonInfo locationsDeliveryTime) {
        return !(checkedLocations.contains(locationsDeliveryTime.getStart())
                || checkedLocations.contains(locationsDeliveryTime.getFinish()));
    }

    private void findDeliveryTime(
            List<LocationDeliveryTimeCommonInfo> previousLocations, long currentLocationId, List<Long> checkedLocations) {
        if (currentLocationId == finish) {
            addLocationToResult(previousLocations);
            return;
        }

        List<LocationDeliveryTimeCommonInfo> dtos = getConnectedNotCheckedLocations(currentLocationId, checkedLocations);
        if (dtos.isEmpty()) {
            return;
        }

        dtos.forEach(
                currentDto ->
                        continueCheckingLocations(previousLocations, currentDto, currentLocationId, checkedLocations));
    }

    private void addLocationToResult(List<LocationDeliveryTimeCommonInfo> locationsDeliveryTimeList) {
        long fullDeliveryTime = locationsDeliveryTimeList.stream().mapToLong(LocationDeliveryTimeCommonInfo::getTimeInMinutes).sum();
        if (minTimeInMinutes != -1) {
            if (minTimeInMinutes > fullDeliveryTime) {
                result = new ArrayList<>();
                minTimeInMinutes = fullDeliveryTime;
                addToResult(locationsDeliveryTimeList, fullDeliveryTime);
            } else if (minTimeInMinutes == fullDeliveryTime) {
                addToResult(locationsDeliveryTimeList, fullDeliveryTime);
            }
        } else {
            minTimeInMinutes = fullDeliveryTime;
            addToResult(locationsDeliveryTimeList, fullDeliveryTime);
        }
    }

    private void addToResult(List<LocationDeliveryTimeCommonInfo> locationsDeliveryTimeList, long fullDeliveryTime) {
        FoundLocationDeliveryTime dto = FoundLocationDeliveryTime.builder().locations(locationsDeliveryTimeList).build();
        dto.setFullDeliveryTimeInMinutes(fullDeliveryTime);
        result.add(dto);
    }

    private void continueCheckingLocations(
            List<LocationDeliveryTimeCommonInfo> previous,
            LocationDeliveryTimeCommonInfo current,
            long currentLocationId,
            List<Long> checkedLocations) {
        List<LocationDeliveryTimeCommonInfo> locations = new ArrayList<>(previous);
        locations.add(current);
        findDeliveryTime(
                locations,
                getNextLocationToCheck(current, currentLocationId),
                markLocationAsChecked(currentLocationId, checkedLocations));
    }

    private long getNextLocationToCheck(LocationDeliveryTimeCommonInfo locationsDeliveryTime, long start) {
        return start == locationsDeliveryTime.getStart()
                ? locationsDeliveryTime.getFinish()
                : locationsDeliveryTime.getStart();
    }

    private List<Long> markLocationAsChecked(long id, List<Long> checkedLocations) {
        List<Long> ids = new ArrayList<>(checkedLocations);
        ids.add(id);
        return ids;
    }

}
