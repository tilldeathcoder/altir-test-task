package com.altir.service.location.cache;

import com.altir.persistence.location.*;
import com.altir.persistence.location.entity.*;
import com.altir.service.location.*;
import com.altir.service.location.model.*;
import lombok.*;
import lombok.extern.slf4j.*;
import org.apache.commons.lang3.tuple.*;
import org.springframework.boot.context.event.*;
import org.springframework.context.*;
import org.springframework.stereotype.*;

import java.util.*;

import static java.util.Collections.*;
import static java.util.Objects.*;
import static org.apache.commons.lang3.ObjectUtils.*;

@Service
@Slf4j
@RequiredArgsConstructor
public class LocationDeliveryTimeInMemoryCache implements ApplicationListener<ApplicationStartedEvent> {

    private final LocationRepository locationRepository;
    private final LocationDeliveryTimeRepository locationDeliveryTimeRepository;

    private static final Map<Pair<Long, Long>, List<FoundLocationDeliveryTime>> LOCATION_DELIVERY_TIME_CACHE = new HashMap<>();

    @Override
    public void onApplicationEvent(ApplicationStartedEvent event) {
      log.info("Initializing location delivery time cache");

      List<Location> allLocations = locationRepository.findAll();
      log.info("Locations count: {}", allLocations.size());

      List<LocationDeliveryTimeCommonInfo> locationDeliveryTimeList = locationDeliveryTimeRepository.findAll().stream().map(LocationDeliveryTimeCommonInfo::new).toList();
      log.info("Connections between locations: {}", locationDeliveryTimeList.size());

      log.info("Searching delivery time between connections:");
        for (int i = 0; i < allLocations.size(); i++) {
            Location start = allLocations.get(i);
            for (int j = i + 1; j < allLocations.size(); j++) {
                Location finish = allLocations.get(j);
                log.info("start: {}, finish: {}", start.getName(), finish.getName());
                LocationDeliveryTimeSearcher searcher = new LocationDeliveryTimeSearcher(locationDeliveryTimeList, start.getId(), finish.getId());
                List<FoundLocationDeliveryTime> result = searcher.search();
                if (isNotEmpty(result)) {
                    log.info("Result: {}", result);
                    LOCATION_DELIVERY_TIME_CACHE.put(Pair.of(start.getId(), finish.getId()), result);
                } else {
                    log.info("Not connected");
                }
            }
        }
        log.info("Cache size: {}", LOCATION_DELIVERY_TIME_CACHE.size());
    }

    public List<FoundLocationDeliveryTime> get(long start, long finish) {
        List<FoundLocationDeliveryTime> result = LOCATION_DELIVERY_TIME_CACHE.get(Pair.of(start, finish));
        if (nonNull(result)) {
            return result;
        }

        List<FoundLocationDeliveryTime> reversedResult = LOCATION_DELIVERY_TIME_CACHE.get(Pair.of(finish, start));
        if (nonNull(reversedResult)) {
            return reversedResult;
        }

        return emptyList();
    }
}
