package com.altir.service.exception;

import lombok.*;

@Getter
public class ProductDeliveryException extends RuntimeException{

    private final long customerId;

    public ProductDeliveryException(long customerId) {
        super("The delivery is not possible for customer");
        this.customerId = customerId;
    }
}
