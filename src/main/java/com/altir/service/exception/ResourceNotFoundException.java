package com.altir.service.exception;

import lombok.*;

import static java.lang.String.*;

@Getter
public class ResourceNotFoundException extends RuntimeException{

    private final String resource;
    private final String qualifier;

    public ResourceNotFoundException(String resource, Object qualifier) {
        super("Resource was not found");
        this.resource = resource;
        this.qualifier = valueOf(qualifier);
    }

    public static ResourceNotFoundException createLocationNotFoundException(Object value) {
        return new ResourceNotFoundException("location", value);
    }

    public static ResourceNotFoundException createCustomerNotFoundException(Object value) {
        return new ResourceNotFoundException("customer", value);
    }

    public static ResourceNotFoundException createProductNotFoundException(Object value) {
        return new ResourceNotFoundException("product", value);
    }
}
