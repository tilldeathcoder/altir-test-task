package com.altir.service.exception;

import lombok.*;

@Getter
public class NoAvailableProductInWarehousesException extends RuntimeException{

    private final long productId;

    public NoAvailableProductInWarehousesException(long productId) {
        super("The product is not available");
        this.productId = productId;
    }
}
