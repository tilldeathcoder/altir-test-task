package com.altir.service.warehouse.model;

import com.altir.persistence.warehouse.entity.*;
import com.altir.service.location.model.*;
import lombok.*;
import lombok.experimental.*;

@Data
@SuperBuilder
public class WarehouseCommonInfo {

    private long id;
    private String  title;
    private LocationCommonInfo location;

    public WarehouseCommonInfo(Warehouse warehouse) {
        id = warehouse.getId();
        title = warehouse.getTitle();
        location = new LocationCommonInfo(warehouse.getLocation());
    }
}
