package com.altir.service.warehouse;

import com.altir.persistence.warehouse.*;
import com.altir.persistence.warehouse.entity.*;
import com.altir.service.exception.*;
import com.altir.service.location.cache.*;
import com.altir.service.location.model.*;
import lombok.*;
import org.springframework.stereotype.*;

import java.util.*;

import static com.altir.service.exception.ResourceNotFoundException.*;
import static org.apache.commons.lang3.ObjectUtils.*;

@Service
@RequiredArgsConstructor
public class WarehouseService {

    private static final long DELIVERY_TIME_FOR_SAME_LOCATION_WAREHOUSE = -1;

    private final WarehouseRepository warehouseRepository;
    private final WarehousesToCustomersRepository warehousesToCustomersRepository;
    private final LocationDeliveryTimeInMemoryCache locationDeliveryTimeInMemoryCache;

    public WarehousesToCustomers findNearestWarehouseForCustomerOrThrowException(List<Long> warehousesIds, long customerId) {
        List<WarehousesToCustomers> result = warehousesToCustomersRepository.findByIdsAndCustomerId(warehousesIds, customerId);
        if (isEmpty(result)) {
            throw new ProductDeliveryException(customerId);
        }

        return result.get(0);
    }

    public Warehouse findByIdOrThrowException(long id) {
        return warehouseRepository.findById(id).orElseThrow(() -> createLocationNotFoundException(id));
    }

    public void addWarehousesToCustomer(long customerId, long customerLocationId) {
        List<Warehouse> all = warehouseRepository.findAll();
        List<WarehousesToCustomers> warehousesToCustomersList = new ArrayList<>();
        all.forEach(warehouse -> {
            long fullDeliveryTime = DELIVERY_TIME_FOR_SAME_LOCATION_WAREHOUSE;
            if (warehouse.getLocation().getId() != customerLocationId) {
                List<FoundLocationDeliveryTime> foundLocationDeliveryTimes = locationDeliveryTimeInMemoryCache.get(customerLocationId, warehouse.getLocation().getId());
                if (isNotEmpty(foundLocationDeliveryTimes)) {
                    fullDeliveryTime = foundLocationDeliveryTimes.get(0).getFullDeliveryTimeInMinutes();
                } else {
                    return;
                }
            }
            warehousesToCustomersList.add(WarehousesToCustomers.builder()
                    .fullDeliveryTimeInMinutes(fullDeliveryTime)
                    .id(WarehousesToCustomersCompositeKey.builder()
                            .warehouseId(warehouse.getId())
                            .customerId(customerId).build())
                    .build());
        });

        warehousesToCustomersRepository.saveAll(warehousesToCustomersList);
    }

}
